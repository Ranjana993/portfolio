import "./Header.css"
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import InstagramIcon from '@mui/icons-material/Instagram';



const Sidebar = ({ open }) => {
    return (
        <>
            <div className={open ? "side on" : "side"}>
                <a href="#" className="close">
                    close icon here
                </a>
                <div className="widget">
                    <div className="logo">
                        <img src="logo" alt="logo" />
                    </div>
                    <ul className="side-nav">
                        <li>
                            <a href="#">Home Light</a>
                        </li>
                        <li>
                            <a href="#">Home Animated</a>
                        </li>
                        <li>
                            <a href="#">Home Dark</a>
                        </li>
                        <li>
                            <a href="#">Blog</a>
                        </li>
                    </ul>
                </div>
                <div className="widget address">
                    <div>
                        <ul className="side-nav">
                            <li>
                                <div className="side_content">
                                    <p>Address</p>
                                    <strong style={{ color: "black" }}>California , TX 798230</strong>
                                </div>
                            </li>
                            <li>
                                <div className="side_content">
                                    <p>Email</p>
                                    <strong style={{ color: "black" }}>support@gmail.com</strong>
                                </div>
                            </li>
                            <li>
                                <div className="side_content">
                                    <p>Contact</p>
                                    <strong style={{ color: "black" }}>+91 1234567899</strong>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div className="widget newsletter">
                    <h4>Get Subscribed!</h4>
                    <form action="#">
                        <div className="input_grp">
                            <input type="text" placeholder='enter your Email here' />
                            <span className='submit-btn'>
                                <button type='submit'>Submit</button>
                            </span>
                        </div>
                    </form>
                </div>
                <div className="widget social">
                    <ul className="links">
                        <li>
                            <a href="#"><FacebookIcon className='icon' /></a>
                        </li>
                        <li>
                            <a href="#"> <TwitterIcon className='icon' /></a>
                        </li>
                        <li>
                            <a href="#"><LinkedInIcon className='icon' /></a>
                        </li>
                        <li>
                            <a href="#"><InstagramIcon className='icon' /></a>
                        </li>
                    </ul>
                </div>
            </div>
        </>
    )
}

export default Sidebar