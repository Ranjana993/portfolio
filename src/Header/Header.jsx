import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import "./Header.css"
import MenuIcon from '@mui/icons-material/Menu';
import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';
import Sidebar from './Sidebar';

const url = "https://validthemes.net/site-template/ambrox/assets/img/logo-icon.png"

const Header = () => {
    const [open, setOpen] = useState(false);


    const handleClick = () => {
        console.log(open);
        alert("hii")
        setOpen(!open)
    }


    return (
        <>
            <div className="navbar">
                <div className="logo">
                    <Link>
                        <img src={url} alt="" />
                    </Link>
                </div>
                <div className="info">
                    <ul>
                        <li className="side-menu" onClick={handleClick}>
                            <MenuIcon style={{cursor:"pointer"}}/>
                        </li>
                        <li className="btn-trend">
                            <a href="#">
                                <span style={{ marginRight: '8px', color:"white" }}><QuestionAnswerIcon /> </span>
                                <span style={{ color: "white" }}>  Chat Now </span></a>
                        </li>
                    </ul>
                </div>

            </div>
            {/* sidebar */}
            <Sidebar open={open} />
        </>
    )
}

export default Header