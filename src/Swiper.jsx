import React, { useRef, useState } from 'react';
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';
import PageOne from './components/PageOne';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

import './App.css';

// import required modules
import { Pagination, Navigation } from 'swiper/modules';
import PageTwo from './components/PageTwo';
import PageThree from './components/PageThree';
import PageFour from './components/PageFour';
import PageFive from './components/PageFive';
import PageSix from './components/PageSix';


const  Swipe = () => {
    return (
        <>
            <Swiper
                pagination={{ type: 'fraction', }}
                navigation={true}
                modules={[Pagination, Navigation]}
                className="mySwiper"
            >
                <SwiperSlide> <PageOne /></SwiperSlide>
                <SwiperSlide><PageTwo /></SwiperSlide>
                <SwiperSlide><PageThree/></SwiperSlide>
                <SwiperSlide><PageFour /></SwiperSlide>
                <SwiperSlide><PageFive /></SwiperSlide>
                <SwiperSlide><PageSix /></SwiperSlide>
            </Swiper>
        </>
    );
}

export default Swipe