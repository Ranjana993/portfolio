import './App.css'
import { Routes, Route } from "react-router-dom"
import PageOne from './components/PageOne'
import PageTwo from './components/PageTwo'
import PageThree from './components/PageThree'
import PageFour from './components/PageFour'
import PageFive from './components/PageFive'
import PageSix from './components/PageSix'


function App() {

  return (
    <>
      <Routes>
        <Route path='/' element={<PageOne />} />
        <Route path='/skills' element={<PageTwo />} />
        <Route path='/portfolio' element={<PageThree />} />
        <Route path='/resume' element={<PageFour />} />
        <Route path='/myblog' element={<PageFive />} />
        <Route path='/contact' element={<PageSix />} />
      </Routes>

    </>
  )
}

export default App
