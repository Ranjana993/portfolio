import Header from '../Header/Header'
import "../styles/PageOne.css"
import { Typewriter } from 'react-simple-typewriter'

const PageOne = () => {

  return (
    <>
      <div className="wrapper">
        <Header />
        <div className="container">
          <div className="content">
            <div className="left_side_container">
              <div className='inner_content'>
                <h2>Hello <img src="https://validthemes.net/site-template/ambrox/assets/img/shape/4.png" alt="" /> </h2>
                <h1> I&apos;m  Easton </h1> <br />
                <h3 style={{ paddingTop: '1rem', margin: 'auto 0', fontWeight: 'normal' }}>
                  <span style={{ color: 'white ', fontWeight: 'bold', borderBottom: '2px solid red' }}>
                    <Typewriter
                      words={['Web developer', 'Professional Coder', 'UI/UX Designer', 'Full Stack Engineer']}
                      loop={5}
                      cursor
                      typeSpeed={70}
                      deleteSpeed={50}
                      delaySpeed={1000}
                    />
                  </span>
                </h3>
                <button className='btn'>Resume</button>
              </div>
            </div>
            <div className="right_side_container">
              <img src="https://validthemes.net/site-template/ambrox/assets/img/illustration/3.png" alt="user_image" />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}


export default PageOne