import React from 'react'
import "../styles/PageFive.css"
import Header from '../Header/Header'
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';

const PageFive = () => {
    return (
        <>
            <div className="blog_wrapper">
                <Header />
                <div className="conatiner">
                    <div className="content">
                        <div className="left">
                            <h2>My Blog </h2>
                        </div>
                        <div className="right">
                            <div className="cards">
                                <div className="card">
                                    <div className="img">
                                        <img src="https://validthemes.net/site-template/ambrox/assets/img/blog/1.jpg" alt="card one" />
                                    </div>
                                    <div className="info">
                                        <h3>Lorem ipsum dolor sit amet consectetur adipisicing..</h3>
                                        <div className="meta">
                                            <ul>
                                                <li className='user_icon_item1'><span> <AccountCircleIcon className="user_icon" /></span><span> User</span></li>
                                                <li className='user_icon_item'><span> <CalendarMonthIcon className="user_icon" /></span><span> 15 August 2023</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="img">
                                        <img src="https://validthemes.net/site-template/ambrox/assets/img/blog/1.jpg" alt="card one" />
                                    </div>
                                    <div className="info">
                                        <h3>Lorem ipsum dolor sit amet consectetur adipisicing..</h3>
                                        <div className="meta">
                                            <ul>
                                                <li className='user_icon_item1'><span> <AccountCircleIcon className="user_icon" /></span><span> User</span></li>
                                                <li className='user_icon_item'><span> <CalendarMonthIcon className="user_icon" /></span><span> 15 August 2023</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="card">
                                    <div className="img">
                                        <img src="https://validthemes.net/site-template/ambrox/assets/img/blog/1.jpg" alt="card one" />
                                    </div>
                                    <div className="info">
                                        <h3>Lorem ipsum dolor sit amet consectetur adipisicing..</h3>
                                        <div className="meta">
                                            <ul>
                                                <li className='user_icon_item1'><span> <AccountCircleIcon className="user_icon" /></span><span> User</span></li>
                                                <li className='user_icon_item'><span> <CalendarMonthIcon className="user_icon" /></span><span> 15 August 2023</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}

export default PageFive