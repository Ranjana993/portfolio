import React from 'react'
import Header from '../Header/Header'
import "../styles/PageFour.css"


const PageFour = () => {
    return (
        <>
            <div className="reume_wrapper">
                <Header />
                <div className="container_resume">
                    <div className="content">
                        <div className="left">
                            <h2>My Resume </h2>
                        </div>
                        <div className="right">
                            <ul>
                                <li>
                                    <h4>A5 - Science & Information </h4>
                                    <h5>SuperKing college </h5>
                                    <span>2000-2001</span>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugiat sequi quis eos animi in iure debitis est alias maiores.</p>
                                </li>
                                <li>
                                    <h4>A5 - Science & Information </h4>
                                    <h5>SuperKing college </h5>
                                    <span>2000-2001</span>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugiat sequi quis eos animi in iure debitis est alias maiores.</p>

                                </li>
                                <li>
                                    <h4>A5 - Science & Information </h4>
                                    <h5>SuperKing college </h5>
                                    <span>2000-2001</span>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugiat sequi quis eos animi in iure debitis est alias maiores.</p>

                                </li>
                                <li>
                                    <h4>A5 - Science & Information </h4>
                                    <h5>SuperKing college </h5>
                                    <span>2000-2001</span>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugiat sequi quis eos animi in iure debitis est alias maiores.</p>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default PageFour