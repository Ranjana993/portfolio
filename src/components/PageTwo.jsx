import React from 'react'
import Header from '../Header/Header'
import "../styles/PageTwo.css"
import { Link } from 'react-router-dom'

const PageTwo = () => {
  return (
    <>
      <div className="skill_wrapper">
        <Header />
        <div className="container">
          <div className="content">
            <div className="skill">
              <h2>My Skills </h2>
            </div>
            <div className="cards">
              <div className="singleCard">
                <div className="card_wrapper">
                  <img src="https://validthemes.net/site-template/ambrox/assets/img/icon/1.png" alt="icon" />
                  <h2> <Link to="#"> Website Design</Link>  </h2>
                  <p style={{fontSize:'14px'}}>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium neque perferendis expedita reiciendis rerum nostrum, accusantium doloremque vero odit, voluptatem voluptatibus, amet hic quis veritatis architecto libero autem iure incidunt.
                  </p>
                </div>
              </div>
              <div className="singleCard">
                <div className="card_wrapper">
                  <img src="https://validthemes.net/site-template/ambrox/assets/img/icon/2.png" alt="icon" />
                  <h2> <Link to="#"> Website Design</Link>  </h2>
                  <p style={{ fontSize: '14px' }}>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium neque perferendis expedita reiciendis rerum nostrum, accusantium doloremque vero odit, voluptatem voluptatibus, amet hic quis veritatis architecto libero autem iure incidunt.
                  </p>
                </div>
              </div>
              <div className="singleCard">
                <div className="card_wrapper">
                  <img src="https://validthemes.net/site-template/ambrox/assets/img/icon/4.png" alt="icon" />
                  <h2> <Link to="#"> Website Design</Link>  </h2>
                  <p style={{ fontSize: '14px' }}>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Praesentium neque perferendis expedita reiciendis rerum nostrum, accusantium doloremque vero odit, voluptatem voluptatibus, amet hic quis veritatis architecto libero autem iure incidunt.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default PageTwo