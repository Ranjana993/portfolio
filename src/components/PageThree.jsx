import React from 'react'
import Header from '../Header/Header'
import "../styles/PageThree.css"
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

const PageThree = () => {
    return (
        <>
            <div className="portfolio_wrapper">
                <Header />
                <div className="port_container">
                    <div className="content">
                        <div className="left_sec">
                            <h2>My Portfolio </h2>
                        </div>
                        <div className="right_section">
                            <div className="cards">
                                <div className="single_card">
                                    <div className="overlay-content">
                                        <img src="https://validthemes.net/site-template/ambrox/assets/img/portfolio/4.jpg" alt="health-fitness" />
                                        <div className="contant">
                                            <div className="title">
                                                <span>Creative</span>
                                                <h5><a href="#">Gaming App</a></h5>
                                            </div>
                                            <a href="#"><ArrowForwardIcon className="forward-arrow" /></a>
                                        </div>
                                    </div>

                                </div>
                                <div className="single_card">
                                    <div className="overlay-content">
                                        <img src="https://validthemes.net/site-template/ambrox/assets/img/portfolio/1.jpg" alt="gaming-app" />
                                        <div className="contant">
                                            <div className="title">
                                                <span>Creative</span>
                                                <h5><a href="#">Gaming App</a></h5>
                                            </div>
                                            <a href="#"><ArrowForwardIcon style={{color:'black'}} className="forward-arrow" /></a>
                                        </div>
                                    </div>

                                </div>
                                <div className="single_card">
                                    <div className="overlay-content">
                                        <img src="https://validthemes.net/site-template/ambrox/assets/img/portfolio/6.jpg" alt="Buisness-app" />
                                        <div className="contant">
                                            <div className="title">
                                                <span>Creative</span>
                                                <h5><a href="#">Gaming App</a></h5>
                                            </div>
                                            <a href="#"><ArrowForwardIcon className="forward-arrow" /></a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </>
    )
}

export default PageThree