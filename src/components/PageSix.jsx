import React, { useState } from 'react';
import "../styles/PageSix.css";
import Header from '../Header/Header';
import EmailIcon from '@mui/icons-material/Email';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import PhoneIcon from '@mui/icons-material/Phone';

const PageSix = () => {
    const [state, setState] = useState({
        name: "",
        email: "",
        phone: "",
        address: ""
    });

    const handleChange = (e) => {
        setState({ ...state, [e.target.name]: e.target.value });
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        alert(state)
    }
    return (

        <>
            {console.log(state.name)}
            <div className="contact_container">
                <Header />
                <div className="contact_wrapper">
                    <div className="container">
                        <div className="content">
                            <div className="left_side">
                                <div className="form">
                                    <h2>Let&apos;s talk</h2>
                                    <form onSubmit={handleSubmit}>
                                        <div className="data">
                                            <input type="text" id='name' name="name" value={state.name} onChange={handleChange} placeholder='Enter Name' />
                                        </div>
                                        <div className="Emaildata">
                                            <input type="text" id='email' name="email" value={state.email} onChange={handleChange} placeholder='Email' />
                                            <input type="text" id='phone' name="phone" value={state.phone} onChange={handleChange} placeholder='Phone' />
                                        </div>
                                        <div className="textdata">
                                            <textarea name="address" id='address' value={state.address} onChange={handleChange} cols="38" rows="5"></textarea>
                                        </div>
                                        <div className="data">
                                            <button type="submit">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="right_side">
                                <div className="email">
                                    <div className="icon"> <EmailIcon /> </div>
                                    <div className="emailinfo">
                                        <h4>Our Email</h4>
                                        <h3>Info@gmail.com</h3>
                                        <h3>Support@gmail.com </h3>
                                    </div>
                                </div>
                                <div className="address">
                                    <div className="icon"><LocationOnIcon /></div>
                                    <div className="addressinfo">
                                        <h4>Address</h4>
                                        <h3>22 Baker Street</h3>
                                        <h3>Support@gmail.com</h3>
                                    </div>
                                </div>
                                <div className="phone">
                                    <div className="icon"><PhoneIcon /></div>
                                    <div className="phoneInfo">
                                        <h4>Phone</h4>
                                        <h3>+44-20-7328-4499</h3>
                                        <h3>+44-20-7328-4499</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default PageSix;
